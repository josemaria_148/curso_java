

import java.util.Scanner;

class PideNumsB {

public static void main(String[] args) {
    
    Scanner keyboard = new Scanner(System.in);
    int total = 0;
    int num=1; //aquí hemos cambiado el 0 por 1, la cuestion es que al meter una letra como primer num no se salga del bucle
    do {
        System.out.printf("Entra un num: ");
        try {  
                num = keyboard.nextInt(); 
                total += num; 
            } 
            
        catch (Exception e) {  
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            } 
    } while (num>0);

    System.out.println("La suma es "+ total);
    keyboard.close();
}

}