
import java.util.Scanner;
import java.util.Random;

class adivinaVariante {

public static void main(String[] args) {
    
    Scanner keyboard = new Scanner(System.in);
    int num = 0; 
    int counter = 0;
    Random random = new Random();
    int incognita = random.nextInt(10)+1;

    
    do {
        System.out.printf("Adivina un número entre 1 y 10: ");
        try {  
                num = keyboard.nextInt(); 
                counter++;  

                if(num==incognita){

                    System.out.println("¡Correcto! El número total de intentos ha sido " + counter);
                    keyboard.close();     
                    break;
                }

            } 
            
        catch (Exception e) {  
                System.out.println("***Dato incorrecto***");
                keyboard.next();
            } 
    } while (num>0 && num<11);

    
    
}

}