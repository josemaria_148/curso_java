
import java.util.Scanner;

class InfoNumsK {

public static void main(String[] args) {
    
    Scanner keyboard = new Scanner(System.in);
    int total = 0;
    int num = 1; 
    //aquí hemos cambiado el 0 por 1, la cuestion es que al meter una letra como primer num
    //no se salga del bucle.  De esta manera, si el primer número introducido es incorrecto, el valor de
    //num será 1, y no saldrá del bucle while
    int counter = 0;
    float media;
    boolean primero = true;
    int maximo=0;
    int minimo=0;
    int counterSin0;
    
    do {
        System.out.printf("Entra un num: ");
        try {  
                num = keyboard.nextInt(); 
                total += num; 
                counter++;
                
                if(primero){
                    maximo = num;
                    minimo = num;
                    primero = false;
                } 

                if(num < minimo && num != 0)
			    {
				minimo = num;
                }
                
			    if(num > maximo)
			    {
				maximo = num;
                }            

            } 
            
        catch (Exception e) {  
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            } 
    } while (num>0);

    counterSin0=counter-1;
    media=(float)total/counterSin0;

    System.out.println("El número total de números introducidos es " + counterSin0);
    System.out.println("La suma es "+ total);
    System.out.println("La media artimética es " + media);
    System.out.println("El valor máximo es " + maximo);
    System.out.println("El valor mínimo es " + minimo);
    
    keyboard.close();
}

}