
import java.util.Scanner;

class Test {

    public static void main(String[] args) {

        String nombre;
        String genero;
        int edad;
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Nombre?");
        nombre = keyboard.nextLine();

        System.out.println("H/M?");
        genero = keyboard.nextLine();

        System.out.println("Edad?");
        edad = keyboard.nextInt();

        if (genero.equals("M")) {

            Persona p = new Mujer(nombre, edad);
            System.out.println(p.presentacion());

        } else {

            Persona p = new Hombre(nombre, edad);
            System.out.println(p.presentacion());

        }

    }

}