
import java.util.Scanner;

public class Persona {

    String nombre;
    int edad;

    protected Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {

        return this.nombre;

    }

    public void setNombre(String nombre) {

        this.nombre = nombre;

    }

    public int getEdad() {

        return this.edad;

    }

    public void setEdad(int edad) {

        this.edad = edad;

    }

    public String presentacion() {

        return "Hola, soy " + this.getNombre() + " y tengo " + this.getEdad() + " años.";

    }

}