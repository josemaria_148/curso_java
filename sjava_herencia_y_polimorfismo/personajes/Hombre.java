 
 public class Hombre extends Persona {

    public Hombre(String nombre, int edad) {
        super(nombre,edad);
    }

    @Override
    public String getNombre() {

        return "el Sr. " + this.nombre;

    }

 }