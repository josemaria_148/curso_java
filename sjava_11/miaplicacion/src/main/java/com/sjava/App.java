package com.sjava;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        new Persona("ana", 1995, "ana@gmail.com");
        new Persona("claudia", 1999, "claudia@gmail.com");
        PersonaController.muestraContactos();
        System.out.println("-----------------------");
        PersonaController.muestraContactoId(2);
        PersonaController.eliminaContacto(2);
        System.out.println("-----------------------");
        PersonaController.muestraContactos();
    }
}
