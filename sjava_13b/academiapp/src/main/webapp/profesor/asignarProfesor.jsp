<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%

    String idcurso = request.getParameter("idcurso");
    String idprofesor = request.getParameter("idprofesor");

    CursoController.asignaProfesorCurso(Integer.parseInt(idcurso), Integer.parseInt(idprofesor));

    response.sendRedirect("/academiapp/curso/list.jsp");

%>