package com.sjava.web;

public class Alumno {

    private int id;
    private String nombre;
    private String email;
    private String telefono;

    public Alumno(String nombre, String email, String telefono) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Alumno(int id, String nombre, String email, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getEmail(){
        return this.email;
    }

    protected void setEmail(String email){
        this.email = email;
    }

    public String getTelefono(){
        return this.telefono;
    }
    
    protected void setTelefono(String telefono){
        this.telefono = telefono;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.email);
    }
}