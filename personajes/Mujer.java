
 public class Mujer extends Persona {

    public Mujer(String nombre, int edad) {
        super(nombre,edad);
    }

    @Override
    public String getNombre() {

        return "la Sra. " + this.nombre;

    }

    @Override
    public int getEdad() {

        if (this.edad > 45){

            this.edad = this.edad-5;
            return this.edad;

        } else {

        return this.edad;

        }

    }

}