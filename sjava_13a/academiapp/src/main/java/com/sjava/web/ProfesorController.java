package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class ProfesorController{

    private static List<Profesor> listaProfesores = new ArrayList<Profesor>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Profesor> getAll(){
        return listaProfesores;
    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        for (Profesor pro : listaProfesores) {
            if (pro.getId()==id){
                return pro;
            }
        }
        return null;
    }
   
    //save guarda un profesor
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Profesor pro) {
        if (pro.getId() == 0){
            contador++;
            pro.setId(contador);
            listaProfesores.add(pro);
        } else {
            for (Profesor proi : listaProfesores) {
                if (proi.getId()==pro.getId()) {
                   
                    proi.setNombre(pro.getNombre());
                    proi.setEmail(pro.getEmail());
                    proi.setTelefono(pro.getTelefono());
                    proi.setEspecialidad(pro.getEspecialidad());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de profesores
    public static int size() {
        return listaProfesores.size();
    }


    // removeId elimina profesor por id
    public static void removeId(int id){
        Profesor borrar=null;
        for (Profesor p : listaProfesores) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaProfesores.remove(borrar);
        }
    }

}