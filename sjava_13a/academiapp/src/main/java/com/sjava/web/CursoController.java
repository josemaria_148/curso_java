package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CursoController{

    private static List<Curso> listaCursos = new ArrayList<Curso>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCursos;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso cur : listaCursos) {
            if (cur.getId()==id){
                return cur;
            }
        }
        return null;
    }
   
    //save guarda un curso
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso cur) {
        if (cur.getId() == 0){
            contador++;
            cur.setId(contador);
            listaCursos.add(cur);
        } else {
            for (Curso curi : listaCursos) {
                if (curi.getId()==cur.getId()) {
                   
                    curi.setNombre(cur.getNombre());
                    curi.setDuracion(cur.getDuracion());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de cursos
    public static int size() {
        return listaCursos.size();
    }


    // removeId elimina curso por id
    public static void removeId(int id){
        Curso borrar=null;
        for (Curso c : listaCursos) {
            if (c.getId()==id){
                borrar = c;
                break;
            }
        }
        if (borrar!=null) {
            listaCursos.remove(borrar);
        }
    }

    public static void asignaProfesorCurso(int idcurso, int idprofesor){

        for (Curso c : listaCursos) {
            if (c.getId()==idcurso){
                c.setIdProfesor(idprofesor); 
                break;
            }
        }
    }



}