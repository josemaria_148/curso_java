package com.sjava.web;

public class Curso {

    int id;
    int idProfesor;
    String nombre;
    int duracion;

    public Curso(String nombre, int duracion) {
        this.nombre = nombre;
        this.duracion = duracion;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método
        // "guarda" de CursoController
        // CursoController.nuevoContacto(this);
    }

    public Curso(int id, String nombre, int duracion) {
        this.id = id;
        this.nombre = nombre;
        this.duracion = duracion;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDuracion() {
        return this.duracion;
    }

    protected void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getId() {
        return this.id;
    }

    protected void setId(int id) {
        this.id = id;
    }

    public int getIdProfesor() {
        return this.idProfesor;
    }

    protected void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

}