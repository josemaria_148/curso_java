
import java.util.Scanner;

class Palindromo{

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        String texto = keyboard.next();
        texto = texto.toLowerCase();

        StringBuilder sb = new StringBuilder();
        
        char[] chars = texto.toCharArray();

        chars[chars.length-1] = Character.toUpperCase(chars[chars.length-1]);

        for (int i = chars.length-1; i >= 0; i--){

            sb.append(chars[i]);
            
            }
        
        System.out.println(sb.toString());

    }
}