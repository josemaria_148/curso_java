package com.sjava.utilidades;

import java.text.Normalizer;

public class Textos {

    public static String capitaliza(String in) {
        return in.substring(0,1).toUpperCase() + in.substring(1).toLowerCase();
    }

    public static String slug(String in) {
        

        String sin_acentos = Normalizer
            .normalize(in, Normalizer.Form.NFD)
            .replaceAll("[^\\p{ASCII}]", "");

        sin_acentos = sin_acentos.toLowerCase().replaceAll("\\s", "_");
        return sin_acentos;

    }
    




}
