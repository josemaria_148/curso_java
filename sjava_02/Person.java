
class Person {
    String nombre;
    int edad;

    Person(String nombre, int edad){
        this.nombre=nombre;
        this.edad=edad;
    }

    void presentacion(){    /*void, no devuelve ningún dato*/ 
        System.out.println("Hola, me llamo " + this.nombre + " y tengo "+ this.edad + " años.");
    }

    public void comparaEdad(Person otra){
        if (this.edad > otra.edad) {
            int diferenciaEdad1;
            diferenciaEdad1 = this.edad - otra.edad;
            System.out.printf(
                "%s es mayor que %s %s años\n",
                this.nombre, otra.nombre, diferenciaEdad1);
        } else if (otra.edad > this.edad){
            int diferenciaEdad2;
            diferenciaEdad2 =  otra.edad - this.edad;
            System.out.printf(
                "%s es mayor que %s %s años\n",
                otra.nombre, this.nombre, diferenciaEdad2);
        } else {
            System.out.printf(
            "%s y %s tienen la misma edad\n",
            this.nombre, otra.nombre);

        }
    }

}


/*Person(String n, int e){
    nombre = n;
    edad = e;
}*/