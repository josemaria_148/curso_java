//Números de números introducidos, valor máximo, valor mínimo y media aritmética.

class InfoNums {

    public static void main(String[] args) {
        int total=0;
        float media;
        int counter=0;
        int maximo=Integer.parseInt(args[0]);
        int minimo=Integer.parseInt(args[0]);

        
         for (int i = 0; i<args.length; i++){
            
            int num = Integer.parseInt(args[i]);

            total += num;
            counter++;

            if(num < minimo)
			{
				minimo = num;
			}
			if(num > maximo)
			{
				maximo = num;
            }
                     
        }    
        
        media=(float)total/counter;

        System.out.println("El número total de números introducidos es " + counter);
        System.out.println("El valor máximo es " + maximo);
        System.out.println("El valor mínimo es " + minimo);
        System.out.println("La media artimética es " + media);
    }
}