package com.sjava.web;

import java.util.ArrayList;

public class PersonaController {
    
    private static ArrayList<Persona> contactos = new ArrayList<Persona>();
    private static int contador=0;

    static {

        new Persona("javier", 20, "javier@gmail.com");
        new Persona("ana", 33, "ana@gmail.com");
        new Persona("maría", 44, "maria@gmail.com");

    }

    public static Persona getContactoId(int id){
        for (Persona p : contactos) {
            if (p.getId()==id) {
                return p;
            }
        }
        return null;
    }

    
    public static void muestraContactos(){
        for (Persona p : contactos) {
            System.out.println(p);
        }
    }
    
    public static ArrayList<Persona> getContactos(){
        return contactos;
    }

    public static void nuevoContacto(Persona pers) {
        contador++;
        pers.setId(contador);
        contactos.add(pers);
    }

    public static int numContactos() {
        return contactos.size();
    }


}