<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.Persona" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>

<%
    String titulo = "Mi primer JSP";
    String[] lista = new String[] {"primero", "segundo", "tercero"};

    Persona paciente1 = new Persona("esteban", 77);
    Persona paciente2 = new Persona("manuel", 33);
    Persona paciente3 = new Persona("tenorio", 44);

    List<Persona> listado = new ArrayList<Persona>();
    listado.add(paciente1);
    listado.add(paciente2);
    listado.add(paciente3);

%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
<h1><% out.print(titulo); %></h1>
<h1><%= titulo %></h1>
<br>
<ul>
    <% for (String s : lista) { %>
        <li><%= s %></li>
    <% } %>
</ul>

<table border="1" width="50%">
<tr><th width="60%">Nombre</th><th width="40%">Edad</th></tr>

<% for(Persona p : listado) { %>
<tr>
    <td><%= p.nombre %></td>
    <td><%= p.edad %></td>
</tr>
<% } %>
</table>

</body>
</html>