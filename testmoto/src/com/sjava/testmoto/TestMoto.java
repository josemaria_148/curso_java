
package com.sjava.testmoto;

class TestMoto {
    public static void main(String[] args) {
        Moto moto1 = new Moto("Yamaha", "MT07", 689, 68, 7250 );
        Moto moto2 = new Moto("Kawasaki", "Ninja 650", 649, 75, 6699 );
        
        moto1.comparaCv(moto2);

        moto1.comparaPvp(moto2);
    }
}
