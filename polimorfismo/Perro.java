
public class Perro extends Animal {

    public String nombre = "Scooby";

    public String getNombre() {

        return nombre;

    }

    //Primer caso

    /**@Override
    public int getNumeroPatas() {

        return 5;

    }*/

    //Segundo caso

    @Override
    public int getNumeroPatas() {

        return super.getNumeroPatas() + 1;

    }

    public void setNombre(String nombre) {

        this.nombre = nombre;

    }

    public static String tipo() {

        return "perro";

    }

}