
public class Animal {

    public String nombre = "Ninguno";

    public String getNombre() {

        return nombre;

    }

    public int getNumeroPatas() {

        return 2;

    }

    public void setNombre(String nombre) {

        this.nombre = nombre;

    }

    public static String tipo() {

        return "animal";

    }

}