package com.sjava.web;

import java.util.Date;

public class Alumno {

    private int id;
    private String nombre;
    private String email;
    private String telefono;
    private Date alta;

    public Alumno(String nombre, String email, String telefono) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public Alumno(String nombre, String email, String telefono, Date alta) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.alta = alta;
    }


    public Alumno(int id, String nombre, String email, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public Alumno(int id, String nombre, String email, String telefono, Date alta) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.alta = alta;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getEmail(){
        return this.email;
    }

    protected void setEmail(String email){
        this.email = email;
    }


    public Date getAlta(){
        return this.alta;
    }

    protected void setAlta(Date alta){
        this.alta = alta;
    }


    public String getTelefono(){
        return this.telefono;
    }
    
    protected void setTelefono(String telefono){
        this.telefono = telefono;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }


   
    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.email);
    }
  
}