import java.util.Date;

class Test {

    public static void main(String[] args) {

        // Ejercicio 1. Muestra año, mes y día
        Date d = dateUtil.createDate(2018, 6, 9);
        System.out.println(d);

        // Ejercicio 2. Muestra año, mes, día, hora, minutos y segundos
        Date d2 = dateUtil.createDateTime(2018, 6, 9, 22, 15, 45);
        System.out.println(d2);


        // Ejercicio 3. Muestra año, mes y día dándole un formato determinado
        StringBuilder sb = new StringBuilder();
        sb.append("La fecha es ");
        sb.append(dateUtil.formatDate(d, "d"));
        sb.append(" de ");
        sb.append(dateUtil.formatDate(d, "MMMM"));
        sb.append(" de ");
        sb.append(dateUtil.formatDate(d, "yyyy"));

        System.out.println(sb.toString());


        // Ejercicio 4. Muestra el número de domingos del mes de Junio de 2018 (los que hemos definido)
        System.out.println(
        dateUtil.cuentaDomingos(2018, 6)
        );

    }

}