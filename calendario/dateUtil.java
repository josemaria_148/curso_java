import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class dateUtil{

    private dateUtil(){

    }

    public static Date createDate(int year, int month, int day){

        Calendar cal = Calendar.getInstance();

        cal.set(year, month-1, day);

        return cal.getTime();

    }

    public static Date createDateTime(int year, int month, int day, int hour, int minutes, int seconds){

        Calendar cal = Calendar.getInstance();

        cal.set(year, month-1, day, hour, minutes, seconds);

        return cal.getTime();

    }

    public static String formatDate(Date fecha, String plantilla){

        SimpleDateFormat sdf = new SimpleDateFormat(plantilla);
        return sdf.format(fecha);

    }

    public static int cuentaDomingos(int year, int month){

        int domingos = 0;
        int mesactual= month - 1;

        Calendar cal = Calendar.getInstance();
        cal.set(year, mesactual, 1);

        while(cal.get(Calendar.MONTH)==mesactual) {
            if (cal.get(Calendar.DAY_OF_WEEK)==1){
                domingos++;
                System.out.println(cal.getTime());
            }
            cal.add(Calendar.DATE, 1);
        }

        return domingos;

    }

}