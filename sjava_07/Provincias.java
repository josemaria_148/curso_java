import java.util.Set;
import java.util.TreeSet;


class Provincias{

    public static void main(String[] args) {
        
        Datos basedatos = new Datos();

        String[] provincias = basedatos.getProvincias();

        Set<String> ordenadas = new TreeSet<String>();

        //forma de recorrer los elementos de la lista y asignará a cada elemento la variable provincia
        // mete provincias en treeset, que elimina los datos duplicados
        
        for (String provincia : provincias){
            ordenadas.add(provincia);
        }

        for (String provincia : ordenadas){
            System.out.println(provincia);
        }



    }   

}